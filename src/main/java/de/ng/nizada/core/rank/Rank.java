package de.ng.nizada.core.rank;

import org.bukkit.entity.Player;

public enum Rank {
	
	ADMINISTRATOR("", "", "Admin", "Administrator", "§4", 100),
	DEVELOPER("", "", "Dev", "Developer", "§3", 60),
	MODERATOR("", "", "Mod", "Moderator", "§c", 50),
	SUPPORTER("", "", "Sup", "Supporter", "§b", 40),
	YOUTUBER("", "", "YT", "YouTuber", "§5", 30),
	VIP("", "", "VIP", "VIP", "§6", 20),
	PREMIUM("", "", "Premium", "Premium", "§e", 10),
	PLAYER("", "", "Spieler", "Spieler", "§a", 0);
	
	private String prefix, suffix;
	private String displayName, fullDisplayName;
	private String colorCode;
	private int power;
	
	private Rank(String prefix, String suffix, String displayName, String fullDisplayName, String colorCode, int power) {
		this.prefix = prefix;
		this.suffix = suffix;
		this.displayName = displayName;
		this.fullDisplayName = fullDisplayName;
		this.colorCode = colorCode;
		this.power = power;
	}
	
	public String getPrefix() {
		return prefix;
	}
	
	public String getSuffix() {
		return suffix;
	}
	
	public String getDisplayName() {
		return displayName;
	}
	
	public String getFullDisplayName() {
		return fullDisplayName;
	}
	
	public String getColorCode() {
		return colorCode;
	}
	
	public int getPower() {
		return power;
	}
	
	public static boolean isRankHigher(Player player, Rank rank, boolean equal) {
		return isRankHigher(getRankByPermission(player), rank, equal);
	}
	
	public static boolean isRankHigher(Rank rank, Rank higherRank, boolean equal) {
		if(equal)
			return rank.getPower() >= higherRank.getPower();
		return rank.getPower() > higherRank.getPower();
	}
	
	public static Rank getRankByPermission(Player player) {
		if(player.hasPermission("nizada.administrator"))
			return Rank.ADMINISTRATOR;
		else if(player.hasPermission("nizada.developer"))
			return Rank.DEVELOPER;
		else if(player.hasPermission("nizada.moderator"))
			return Rank.MODERATOR;
		else if(player.hasPermission("nizada.supporter"))
			return Rank.SUPPORTER;
		else if(player.hasPermission("nizada.youtuber"))
			return Rank.YOUTUBER;
		else if(player.hasPermission("nizada.vip"))
			return Rank.VIP;
		else if(player.hasPermission("nizada.premium"))
			return Rank.PREMIUM;
		return Rank.PLAYER;
	}
}